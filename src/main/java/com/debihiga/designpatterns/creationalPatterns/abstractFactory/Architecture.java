package com.debihiga.designpatterns.creationalPatterns.abstractFactory;

public enum Architecture {
    ENGINOLA, EMBER
}
