package com.debihiga.designpatterns.creationalPatterns.prototype;

class Harry implements Person {
    private final String NAME = "harry";

    @Override
    public Person clone() {
        return new Harry();
    }

    @Override
    public String toString() {
        return NAME;
    }
}
