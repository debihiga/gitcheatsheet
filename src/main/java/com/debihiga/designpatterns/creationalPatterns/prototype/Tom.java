package com.debihiga.designpatterns.creationalPatterns.prototype;

class Tom implements Person {
    private final String NAME = "tom";

    @Override
    public Person clone() {
        return new Tom();
    }

    @Override
    public String toString() {
        return NAME;
    }
}
