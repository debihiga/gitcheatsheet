package com.debihiga.designpatterns.creationalPatterns.prototype;

interface Person {
    Person clone();
}
