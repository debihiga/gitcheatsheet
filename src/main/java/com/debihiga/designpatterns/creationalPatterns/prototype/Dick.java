package com.debihiga.designpatterns.creationalPatterns.prototype;

class Dick implements Person {
    private final String NAME = "dick";

    @Override
    public Person clone() {
        return new Dick();
    }

    @Override
    public String toString() {
        return NAME;
    }
}
